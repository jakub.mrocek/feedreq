import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import Divider from '@material-ui/core/Divider';

// test
import Block from './Block';
import TestBlock from './Blocks/TestBlock';
import TagRow from './TagRow';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold'
  },
  dividerPadding: {  
    marginTop: '20px',
    marginBottom: '20px'
  }
});



const FeedCard = ({ name, description, tags, icon }) => {
  const classes = useStyles();

  const [block, setBlock] = useState({
    name: "",
    type: "", 
    description: " ", 
    tags: [],
    cover: ""
  });
  const [cover, setCover] = useState("");

  // testovacie nacitanie testblocku
  useEffect(() => {
    setBlock(TestBlock);
    setCover("https://cdnb.artstation.com/p/assets/images/images/006/897/659/large/mikael-gustafsson-wallpaper-mikael-gustafsson.jpg?1502104559");
  }, []);

  const test = () => {
    let toReturn = [];

    for (let index = 0; index < 20; index++) {
      toReturn.push({ id: index, text: `test${index}`, color: "" });
    }

    return toReturn;
  }

  const toReturn = test();

  

  return (
    
    
    <Card className={classes.root}>
      {cover.length > 0 &&
      <CardMedia
          component="img"
          alt="Cover"
          height="140"
          image={cover}
          title="Cover"
        />
      }
      <CardContent>
      
      <Typography className={classes.title} color="textPrimary" gutterBottom>{icon.length > 0 && <Icon>{icon}</Icon>} Test</Typography>
      {tags.length > 0 &&
        <TagRow tagsData={tags} />
      } 
      <Divider className={classes.dividerPadding} />

      <Block name={block.name} type={block.type} description={block.description} tags={block.tags}><p>blablabla</p></Block>
      </CardContent>
      <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions>
    </Card>
  );
}

export default FeedCard;
