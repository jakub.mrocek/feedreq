import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Tag from './Tag';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

function TagRow({ tagsData }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {tagsData.map(tag => (
        <Tag key={tag.id} text={tag.text} color={tag.color} />
      ))}
    </div>
  )
}

export default TagRow;
