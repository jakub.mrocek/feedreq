import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { string, array } from 'prop-types';

import TagRow from './TagRow';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  description: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12,
  },
});

const Block = ({ name, type, description, tags, children }) => {
  const classes = useStyles();

  return (
    <div>
      <Typography className={classes.title} color="textPrimary" gutterBottom>
        {name.length > 0 ? name : type}
      </Typography>
      <Typography className={classes.description} color="textSecondary" gutterBottom>
        {description.length > 0 ? description : "No description"}
      </Typography>
      {tags.length > 0 &&
        <TagRow tagsData={tags} />
      } 
      {children}
    </div>
  )
}

Block.propTypes = {
  name: string,
  description: string,
  tags: array
}

export default Block;
