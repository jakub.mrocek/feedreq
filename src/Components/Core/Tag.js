import React from 'react';
import Chip from '@material-ui/core/Chip';


function Tag({ text, color }) {
  return (
    <Chip label={text} color={color.length > 0 ? color : "default"} />
  )
}

export default Tag;
