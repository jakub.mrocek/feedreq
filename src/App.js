import React from 'react';
import FeedCard from './Components/Core/FeedCard';
import Container from '@material-ui/core/Container';

function App() {
  return (
    <React.Fragment>
      <Container fixed>
        <FeedCard tags={[ { id: 0, text: `test`, color: "" } ]} icon="android" />
      </Container>
    </React.Fragment>
  );
}

export default App;
